
FUNCTION BRUSwidget_Listbox : USINT (*Formats data into a string which can be used by a Listbox widget*)
	VAR_INPUT
		newParam : BOOL;
	END_VAR
END_FUNCTION

{REDUND_ERROR} FUNCTION BRUSwidget_PaperTrf : UINT (*Creates a transformation string for an svg object. *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pStr : UDINT; (*Address of string bound to the Transform property of the paper widget ><br> Pointer to STRING <br><br>
<b>IMPORTANT:</b> User must ensure that the size of the string passed in is large enough to accomodate all of the needed transformations!<br>Recommended starting size: STRING[1000]*) (* *) (*#PAR*)
		pTrf : REFERENCE TO svgTrf_typ; (*Transformation parameter structure.<br> Pointer to <b><a href="../Data%20types%20and%20constants/Data%20types/svgTrf_typ.html">svgTrf_typ</a></b
*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : svgInternalStructure_typ; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION
