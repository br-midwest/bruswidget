
TYPE
	svgTrf_typ : 	STRUCT  (*Full collection of transformation parameters*)
		object : STRING[80]; (*ID of the svg object to be transformed. Passed in as STRING*)
		duration : UINT := 50; (*Duration of the requested transformation in milliseconds*)
		display : BOOL := TRUE; (*Whether or not object is displayed <br><br>
<b>TRUE</b>--> Displayed <br>
<b>FALSE</b>--> Hidden <br>*)
		first : BOOL := TRUE; (*Clears the string and starts it with '['<br><br>
If only one svg object will be transformed in the paper:<br>
set to <b>TRUE</b> <br><br>
If multiple objects will be transformed in a single paper:<br>
set to <b>TRUE</b> for the first object <br>
set to <b>FALSE</b> for all other objects*)
		last : BOOL := TRUE; (*Adds ']' to the end of the string <br><br>
If only one svg object will be transformed in the paper:<br>
set to <b>TRUE</b> <br><br>
If multiple objects will be transformed in a single paper:<br>
set to <b>TRUE</b> for the last object <br>
set to <b>FALSE</b> for all other objects*)
		translate : svgTrf_translate_typ; (*Parameters for object translation*)
		scale : svgTrf_scale_typ; (*Parameters for object scaling*)
		spin : svgTrf_spin_typ; (*Parameters for object spin*)
		style : svgTrf_style_typ; (*Parameters for object styling*)
		tPath : svgTrf_tpath_typ; (*Parameters for object path following*)
	END_STRUCT;
	svgTrf_translate_typ : 	STRUCT  (*Translation parameters*)
		enable : BOOL := FALSE; (*Determines whether or not a translation is applied to the selected object*)
		x : INT := 0; (*Object goes here, x*)
		y : INT := 0; (*Object goes here, y*)
	END_STRUCT;
	svgTrf_scale_typ : 	STRUCT  (*Scaling parameters*)
		enable : BOOL := FALSE; (*Determines whether or not scaling is applied to the selected object*)
		x : REAL := 1.0; (*Scale factor in the x direction (1.0 is as-rendered)*)
		y : REAL := 1.0; (*Scale factor in the y direction (1.0 is as-rendered)*)
	END_STRUCT;
	svgTrf_spin_typ : 	STRUCT  (*Spin parameters*)
		enable : BOOL := FALSE; (*Determines whether or not spin is applied to the selected object*)
		cy : INT := 0; (*Center of rotation x*)
		cx : INT := 0; (*Center of rotation y*)
		angle : REAL := 0; (*Angle, in degrees, of rotation about center point*)
	END_STRUCT;
	svgTrf_style_typ : 	STRUCT  (*Styling parameters*)
		enable : BOOL := FALSE; (*Determines whether or not style is applied to the selected object*)
		stroke : INT := SVGLIB_IGNORE_STYLE; (*Color index (defined in color list of paper widget) for the object's stroke*)
		strokeWidth : INT := SVGLIB_IGNORE_STYLE; (*Pre-scaled width of the object's stroke in pixels*)
		strokeOpacity : REAL := SVGLIB_IGNORE_STYLE; (*Self-explanitory*)
		strokeDashArray : ARRAY[0..9]OF INT := [10(SVGLIB_IGNORE_STYLE)]; (*Segments the object stroke*)
		fill : INT := SVGLIB_IGNORE_STYLE; (*Color index (defined in color list of paper widget) for the object's fill*)
		fillOpacity : REAL := SVGLIB_IGNORE_STYLE; (*Self-explanitory*)
	END_STRUCT;
	svgTrf_tpath_typ : 	STRUCT  (*tPath parameters*)
		enable : BOOL := FALSE; (*Determines whether or not tpath is applied to the selected object*)
		pathID : STRING[80]; (*Name of the path definition in the SVG content*)
		autorotate : BOOL := FALSE; (*Should the object rotate with the path?*)
		endPcnt : REAL := 0.0; (*Percentage through the path that the object should end at*)
		startPcnt : REAL := 0.0; (*Percentage through the path that the object should begin at*)
	END_STRUCT;
	svgInternalStructure_typ : 	STRUCT  (*Internal function structure - Do not use*)
		strDuration : STRING[10];
		strSpinCx : STRING[10];
		strSpinCy : STRING[10];
		strSpinAngle : STRING[10];
		strTransX : STRING[10];
		strTransY : STRING[10];
		strDisplay : STRING[10];
		strScaleX : STRING[10];
		strScaleY : STRING[10];
		strTPathAutoRot : STRING[10];
		strTPathStart : STRING[10];
		strTPathEnd : STRING[10];
		strStyleStrokeArr : STRING[10];
		strStyleFillOp : STRING[10];
		strStyleFill : STRING[10];
		strStyleStrokeWd : STRING[10];
		strStyleStroke : STRING[10];
		strStyleStrokeOp : STRING[10];
		strStyle : STRING[10];
		da : INT;
		dasharrayReg : BOOL;
	END_STRUCT;
END_TYPE
