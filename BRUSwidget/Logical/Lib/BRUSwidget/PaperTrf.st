FUNCTION BRUSwidget_PaperTrf
	
	IF ADR(pTrf) <> 0 AND pStr <> 0 THEN

		IF pTrf.first THEN
			brsmemset(pStr,0,brsstrlen(pStr));
			brsstrcat(pStr, ADR('['));
		END_IF
		
		// select object
		// proper sytax - "select":"#<objectname>";
		brsstrcat(pStr, ADR('{"select":"#'));
		brsstrcat(pStr, ADR(pTrf.object));
		
		// set duration
		// proper sytax - "duration":<duration>;
		brsstrcat(pStr, ADR('","duration":'));
		IS.strDuration	:= UINT_TO_STRING(pTrf.duration);
		brsstrcat(pStr, ADR(IS.strDuration));
		
		// set display
		// proper sytax - "display":<visibility>;
		brsstrcat(pStr, ADR(',"display":'));
		IF pTrf.display THEN
			IS.strDisplay	:= 'true';
		ELSE
			IS.strDisplay	:= 'false';
		END_IF
		brsstrcat(pStr, ADR(IS.strDisplay));
		
		// do translation
		// proper sytax - "translate":[<x>,<y>];
		IF pTrf.translate.enable THEN
			brsstrcat(pStr, ADR(',"translate":['));
			IS.strTransX	:= INT_TO_STRING(pTrf.translate.x);
			brsstrcat(pStr, ADR(IS.strTransX));
			brsstrcat(pStr, ADR(','));
			IS.strTransY	:= INT_TO_STRING(pTrf.translate.y);
			brsstrcat(pStr, ADR(IS.strTransY));
			brsstrcat(pStr, ADR(']'));
		END_IF
		
		// do spin
		// proper sytax - "spin":[<angle>,<cx>,<cy>];
		IF pTrf.spin.enable THEN
			brsstrcat(pStr, ADR(',"spin":['));
			IS.strSpinAngle	:= REAL_TO_STRING(pTrf.spin.angle);
			brsstrcat(pStr, ADR(IS.strSpinAngle));
			brsstrcat(pStr, ADR(','));
			IS.strSpinCx		:= INT_TO_STRING(pTrf.spin.cx);
			brsstrcat(pStr, ADR(IS.strSpinCx));
			brsstrcat(pStr, ADR(','));
			IS.strSpinCy		:= INT_TO_STRING(pTrf.spin.cy);
			brsstrcat(pStr, ADR(IS.strSpinCy));
			brsstrcat(pStr, ADR(']'));
		END_IF
	
		// do scale
		// proper sytax - "scale":[<scalex>,<scaley>];
		IF pTrf.scale.enable THEN
			brsstrcat(pStr, ADR(',"scale":['));
			IS.strScaleX	:= REAL_TO_STRING(pTrf.scale.x);
			brsstrcat(pStr, ADR(IS.strScaleX));
			brsstrcat(pStr, ADR(','));
			IS.strScaleY	:= REAL_TO_STRING(pTrf.scale.y);
			brsstrcat(pStr, ADR(IS.strScaleY));
			brsstrcat(pStr, ADR(']'));
		END_IF
		
		// do tpath
		// proper sytax - "tPath":["#<pathname>",<autorotate>,<OPTstartpcnt>,<OPTendpcnt>];
		IF pTrf.tPath.enable THEN
			brsstrcat(pStr, ADR(',"tPath":["#'));
			brsstrcat(pStr, ADR(pTrf.tPath.pathID));
			brsstrcat(pStr, ADR('",'));
			
			IF pTrf.tPath.autorotate THEN
				IS.strTPathAutoRot	:= 'true,';
			ELSE
				IS.strTPathAutoRot	:= 'false,';
			END_IF
			brsstrcat(pStr, ADR(IS.strTPathAutoRot));
			
			IS.strTPathStart	:= REAL_TO_STRING(pTrf.tPath.startPcnt);
			brsstrcat(pStr, ADR(IS.strTPathStart));
			brsstrcat(pStr, ADR(','));
			IS.strTPathEnd		:= REAL_TO_STRING(pTrf.tPath.endPcnt);
			brsstrcat(pStr, ADR(IS.strTPathEnd));
			brsstrcat(pStr, ADR(']'));
		END_IF
		
		// do style
		// proper sytax - "style":"<styleproperty>:<styleparameter>;<styleproperty>:<styleparameter>;"
		IF pTrf.style.enable THEN
			brsstrcat(pStr, ADR(',"style":"'));
			
			IF pTrf.style.stroke <> SVGLIB_IGNORE_STYLE THEN
				brsstrcat(pStr, ADR('stroke:'));
				IS.strStyleStroke	:= INT_TO_STRING(pTrf.style.stroke);
				brsstrcat(pStr, ADR(IS.strStyleStroke));
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			IF pTrf.style.strokeWidth <> SVGLIB_IGNORE_STYLE THEN
				brsstrcat(pStr, ADR('stroke-width:'));
				IS.strStyleStrokeWd	:= INT_TO_STRING(pTrf.style.strokeWidth);
				brsstrcat(pStr, ADR(IS.strStyleStrokeWd));
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			IF pTrf.style.strokeOpacity <> SVGLIB_IGNORE_STYLE THEN
				brsstrcat(pStr, ADR('stroke-opacity:'));
				IS.strStyleStrokeOp	:= REAL_TO_STRING(pTrf.style.strokeOpacity);
				brsstrcat(pStr, ADR(IS.strStyleStrokeOp));
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			IS.dasharrayReg	:= FALSE;
			FOR IS.da := 0 TO 9 DO
				IF pTrf.style.strokeDashArray[IS.da] <> SVGLIB_IGNORE_STYLE THEN
					IF NOT IS.dasharrayReg THEN						
						brsstrcat(pStr, ADR('stroke-dasharray:'));
						IS.dasharrayReg	:= TRUE;
					END_IF
					brsstrcat(pStr, ADR(' '));
					IS.strStyleStrokeArr	:= INT_TO_STRING(pTrf.style.strokeDashArray[IS.da]);
					brsstrcat(pStr, ADR(IS.strStyleStrokeArr));
				END_IF
			END_FOR
			
			IF IS.dasharrayReg THEN
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			IF pTrf.style.fill <> SVGLIB_IGNORE_STYLE THEN 
				brsstrcat(pStr, ADR('fill:'));
				IS.strStyleFill	:= INT_TO_STRING(pTrf.style.fill);
				brsstrcat(pStr, ADR(IS.strStyleFill));
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			IF pTrf.style.fillOpacity <> SVGLIB_IGNORE_STYLE THEN
				brsstrcat(pStr, ADR('fill-opacity:'));
				IS.strStyleFillOp	:= REAL_TO_STRING(pTrf.style.fillOpacity);
				brsstrcat(pStr, ADR(IS.strStyleFillOp));
				brsstrcat(pStr, ADR(';'));
			END_IF
			
			brsstrcat(pStr, ADR('"'));
		END_IF
		
		brsstrcat(pStr, ADR('}'));
		
		IF pTrf.last THEN
			brsstrcat(pStr, ADR(']'));
		ELSE
			brsstrcat(pStr, ADR(','));
		END_IF
	ELSE
		BRUSwidget_PaperTrf	:= 50000;
	END_IF
		
END_FUNCTION
