PROGRAM _INIT
  
	// initialize some transformation parameters
	trfsetup.duration	:= 100;
	trfsetup.first		:= TRUE;
	trfsetup.last		:= TRUE;
	trfsetup.object		:= 'mycircle';
	trfsetup.display	:= TRUE;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// define some SVG content, green laser dot
	svgCont	:= 
	'<svg>
		<circle id="mycircle" cx="0" cy="0" r="3" style="fill:#00cc00"/>
	</svg>';
	
	// do some transformations
	trfsetup.translate.enable	:= TRUE;
	trfsetup.translate.x		:= LaserTracer.CurrentPos.x;
	trfsetup.translate.y		:= LaserTracer.CurrentPos.y;
	
	trfsetup.style.enable		:= TRUE;
	trfsetup.style.fill			:= LASER_COLOR;

	// call the function
	svgTrfBasic(ADR(svgTransform), ADR(trfsetup));
	
END_PROGRAM

